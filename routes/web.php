<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\SiteController;

Route::get('api-documentation', [SiteController::class, 'apiDocumentation'])->name('api.documentation');

Route::get('delivery-options-emulator', [SiteController::class, 'deliveryOptionsEmulator'])->name('emulator.delivery_options');
Route::post('delivery-options-emulator-response', [SiteController::class, 'sendShortDeliveryOptions'])->name('emulator.send_delivery_options');
