<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения данных об адресе полученном от Dadata
 *
 * @author Oleg Pyatin
 */
class DadataAddressFields extends BaseDto
{
    /**
     * @var string  Страна
     */
    public $country;
    /**
     * @var string  Почтовый индекс
     */
    public $postal_code;
    /**
     * @var string  Регион адреса
     */
    public $region_with_type;
    /**
     * @var string  Город адреса с обозначением
     */
    public $city_with_type;
    /**
     * @var string  Город адреса без обозначения
     */
    public $city;
    /**
     * @var string  Улица с обозначением
     */
    public $street_with_type;
    /**
     * @var string  Просто название улицы
     */
    public $street;
    /**
     * @var string  Обозначение дома
     */
    public $house_type_full;
    /**
     * @var string  Дом
     */
    public $house;
    /**
     * @var string  Обозначение строения
     */
    public $block_type_full;
    /**
     * @var string  Строение
     */
    public $block;
    /**
     * @var string  ОБозначение квартиры (или схожего помещения, холла или др)
     */
    public $flat_type;
    /**
     * @var string  Квартира
     */
    public $flat;
    /**
     * @var string  КЛАДР города (нужен в дальнейшем для получения кода СДЕК)
     */
    public $city_kladr_id;
}
