<?php

namespace Modules\MyWarehouse\Entities;

/**
 * Класс для хранения вспомогательной информации для работы с системой МойСклад (URL адресов и др)
 *
 * @author Oleg Pyatin
 */
class MwProcessValues
{
    /**
     * URL для получения данных о заказе
     */
    public const GET_ORDER_INFO_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/';
    /**
     * Модификаторы для получения дополнительной информации
     */
    public const EXTRA_ENTITY_INFO = '?expand=organization,agent,positions.assortment';
    /**
     * Сообщение об ошибке в случае невозможности получить данные о тарифе
     */
    public const ERROR_GET_TARIFF_DATA = 'Ошибка в получении данных тарифа';


    /**
     * URL для изменения заказов в МойСклад
     */
    public const MW_ORDER_CHANGE_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/';
    /**
     * URL для изменения атрибутов заказа в МойСклад (например указания ошибок, накладной ТК и пр)
     */
    public const MW_ORDER_CHANGE_ATTR_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/attributes/';
    /**
     * URL для изменения статусов заказов в МойСклад
     */
    public const MW_ORDER_CHANGE_STATE_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/states/';


    /**
     * Начальная часть URL для получения списка заказов
     */
    public const MW_GET_ORDER_LIST_SIZE_URL_BASE_PART = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder?expand=positions,agent,demands';
    /**
     * Часть URL списка заказов с именованием фильтра
     */
    public const MW_GET_ORDER_LIST_URL_YANDEX_TAXI_FILTER = '&filter=state=https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/states/35a7af33-1abe-11ec-0a80-00ef0009b5dd';




    /**
     * Атрибут в Мой Склад для штрихкодов (взято из старой интеграции с Почтой России)
     */
    public const MW_ATTR_RUSPOST_BARCODE = 'e927642b-88b7-11e7-6b01-4b1d00073fdc';
    /**
     * Атрибут в Мой Склад для расчета себестоимости доставки
     */
    public const MW_ATTR_DELIVERY_COST = 'cb8c6079-799d-11e5-7a40-e8970024bde7';
    /**
     * Атрибут в Мой Склад для вывода ошибок
     */
    public const MW_ATTR_ERROR_MESSAGE = 'a31f1635-4fa3-11e9-912f-f3d400039420';
    /**
     * Атрибут в Мой Склад куда указывается ссылка на печать Ф7П
     */
    public const MW_ATTR_F7P_PRINT = 'e9a95f3d-4fa2-11e9-9109-f8fc0003db1e';
    /**
     * Атрибут в Мой Склад для дополнительного статуса ТК
     */
    public const MW_ATTR_ADDITIONAL_STATUS = '33309169-b15c-11ea-0a80-073900091d1d';

    /**
     * Получение сущности дополнительного статуса ТК из Мой Склад
     */
    public const MW_GET_ADDITIONAL_STATUS_URI = 'https://online.moysklad.ru/api/remap/1.2/entity/customentity/df280584-b157-11ea-0a80-051f0008aaf9/33491101-b6bd-11ea-0a80-06f0001feef6';

    /**
     * Параметр временного размера выборки для пакетной загрузки Мой Склад
     */
    public const MW_TEMPORARY_PACKAGE_SELECT_SIZE = 2;
    /**
     * Параметр временного статуса для пакетной выгрузки
     */
    public const MW_TEMPORARY_PACKAGE_STATUS = 'Доставлен';

}
