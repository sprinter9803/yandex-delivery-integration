<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания типа отгрузки в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class CreatePlacesData extends BaseDto
{
    /**
     * @var Идентификатор грузового места в системе партнера
     */
    public $externalId;
    /**
     * @var Вес и габариты отправления
     */
    public $dimensions;
    /**
     * @var Данные о товарах в заказе
     */
    //public $items;
}
