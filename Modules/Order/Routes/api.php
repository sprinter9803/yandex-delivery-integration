<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'order'], function () {
    Route::post('create/{mw_order_uuid}', 'OrderController@create')->name('order.create');
    Route::get('get/{yd_order_uuid}', 'OrderController@getOrderData')->name('order.get');
    Route::post('short_delivery_options', 'OrderController@shortDeliveryOptions')->name('order.short_delivery_options');
    Route::get('delivery_options_erp/{mw_order_uuid}', 'OrderController@deliveryOptionsErp')->name('order.delivery_options_erp');

    Route::get('b2b_get_offers/{mw_order_uuid}', 'OrderController@getOffers')->name('order.b2b_get_offers');
    Route::get('b2b_request_info/{yd_request_id}', 'OrderController@requestInfo')->name('order.b2b_request_info');
    Route::get('b2b_request_history/{yd_request_id}', 'OrderController@requestHistory')->name('order.b2b_request_history');
    Route::get('b2b_cancel_request/{yd_request_id}', 'OrderController@cancelRequest')->name('order.b2b_cancel_request');

    Route::get('b2b_confirm_offer', 'OrderController@confirmOffer')->name('order.b2b_confirm_offer');
    Route::get('b2b_label_create', 'OrderController@createLabel')->name('order.b2b_create_label');

    Route::get('b2b_fast_option', 'OrderController@fastOption')->name('order.b2b_fast_option');
});
