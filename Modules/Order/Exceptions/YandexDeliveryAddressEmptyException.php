<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\YandexDeliveryOrderCreatingException;

class YandexDeliveryAddressEmptyException extends YandexDeliveryOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Адрес населенного пункта не заполнен';
    }
}
