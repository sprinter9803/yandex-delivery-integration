<?php

namespace Modules\YandexDelivery\Entities;

class YandexDeliveryProcessValues
{
    /**
     * URI для формирования черновиков заказов Яндекс.Доставки
     */
    public const YANDEX_DELIVERY_ORDER_CREATE_URI = 'orders';
    /**
     * URI для формирования подтверждения заказов Яндекс.Доставки
     */
    public const YANDEX_DELIVERY_ORDER_AFFIRM_URI = 'orders/submit';
    /**
     * URI для получения данных о заказе в Яндекс.Доставке
     */
    public const YANDEX_DELIVERY_ORDER_GET_INFO_URI = 'orders/';
    /**
     * URI для возможных опций доставки
     */
    public const YANDEX_DELIVERY_OPTIONS_LIST = 'delivery-options';

    /**
     * ID отправки для магазина Braun
     */
    public const YD_BRAUN_SHOP_SENDER_ID = 500004666;
    /**
     * Тип доставки по-умолчанию
     */
    public const YD_DELIVERY_TYPE_DEFAULT = 'COURIER';
    /**
     * Тип контакта (для связи с получателем) Яндекс.Доставки по-умолчанию
     */
    public const YD_CONTACT_TYPE_DEFAULT = 'RECIPIENT';

    /**
     * Способ оплаты Яндекс.Доставки наличными курьеру
     */
    public const YD_PAYMENT_METHOD_CASH = 'CASH';
    /**
     * Способ оплаты Яндекс.Доставки картой курьеру
     */
    public const YD_PAYMENT_METHOD_CARD = 'CARD';
    /**
     * Способ оплаты Яндекс.Доставки предоплатой
     */
    public const YD_PAYMENT_METHOD_PREPAID = 'PREPAID';


    /**
     * UUID атрибута адреса покупателя
     */
    public const ATTR_RECIPIENT_MW_ORDER_ADDR = '2a5b2325-49cf-11e6-7a69-97110010a6c2';

    /**
     * UUID атрибута комментария
     */
    public const MW_ATTR_COMMENT_UUID = '9ffdbf2e-45ac-11e5-7a40-e89700218f1a';
    /**
     * Адрес отправителя (склад на Шаболовской)
     */
    public const YD_DEFAULT_WAREHOUSE_LOCATION = 'г. Москва, ул. Малая Калужская, д. 15, стр. 17';


    /**
     * Способ отгрузки Яндекс.Доставки самостоятельный вариант
     */
    public const YD_SHIPMENT_TYPE_IMPORT = 'IMPORT';
    /**
     * Способ отгрузки Яндекс.Доставки вариант курьером
     */
    public const YD_SHIPMENT_TYPE_WITHDRAW = 'WITHDRAW';
    /**
     * Включаем только варианты на основе установок в личном кабинете
     */
    public const YD_SHIPMENT_DEFAULT_OPTIONS_INCLUDING = false;

    /**
     * Текст ошибки для случая вычисления вариантов доставки
     */
    public const YD_DELIVERY_OPTIONS_ERROR = 'Возникла ошибка при вычислении вариантов доставки';
    /**
     * Текст ошибки для случая вычисления вариантов доставки
     */
    public const YD_DELIVERY_OPTIONS_SUCCESS_SEND = 'Варианты доставки успешно отправлены';
}
