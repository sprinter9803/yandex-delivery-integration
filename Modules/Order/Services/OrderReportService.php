<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Components\MyWarehouseConnector;
use Modules\Order\Entities\OrderProcessValues;
use Modules\Order\Entities\OrderDocumentData;
use Modules\YandexB2B\Components\YandexDeliveryB2BConnector;
use Modules\YandexB2B\Entities\YandexDeliveryB2BProcessValues;
use Modules\YandexB2B\Entities\YandexDeliveryB2bOffer;
use Modules\YandexDelivery\Entities\YandexDeliveryProcessValues;
use Illuminate\Support\Facades\Storage;

/**
 * Сервис для выполнения действий после успешного создания заказа в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class OrderReportService
{
    protected $mw_connector;

    protected $yandex_b2b_connector;

    public function __construct(MyWarehouseConnector $mw_connector, YandexDeliveryB2BConnector $yandex_b2b_connector)
    {
        $this->mw_connector = $mw_connector;
        $this->yandex_b2b_connector = $yandex_b2b_connector;
    }

    /**
     * Выполнение результирующих действия после создания заказа
     *
     * @param string $base_mw_order_number  ID заказа в системе Мой Склад
     * @param string $new_yd_order_id  ID нового созданного заказа в Яндекс.Доставке
     * @return void  Просто выполняем действия и ничего не возвращаем
     */
    public function doFinalActions(string $base_mw_order_number, string $new_yd_order_id)
    {
        $order_document_data = OrderDocumentData::loadFromArray([
            'new_yd_request_id'=>$new_yd_order_id,
        ]);

        $this->saveReportToMw($base_mw_order_number, $order_document_data);
    }

    /**
     * Выполнение результирующих действия после создания заказа от B2B API Яндекса
     *
     * @param string $base_mw_order_number  ID заказа в системе Мой Склад
     * @param string $new_yd_order_id  ID нового созданного заказа в Яндекс.Доставке
     * @return void  Просто выполняем действия и ничего не возвращаем
     */
    public function doFinalB2bActions(string $base_mw_order_number, string $new_yd_order_id)
    {
        $order_document_data = OrderDocumentData::loadFromArray([
            'new_yd_request_id'=>$new_yd_order_id,
            'label'=>$this->getYandexDeliveryLabel($base_mw_order_number, $new_yd_order_id)
        ]);

        $this->saveReportToMw($base_mw_order_number, $order_document_data);
    }

    /**
     * Функция получения ярлыков для сформированного заказа
     *
     * @param string $request_id  ID заказа в системе Яндекс.Доставка B2B
     */
    public function getYandexDeliveryLabel(string $mw_order_uuid, string $request_id)
    {
        $this->syncYandexDeliveryWaiting();

        $pdf = $this->yandex_b2b_connector->processDataQuery(YandexDeliveryB2BProcessValues::YANDEX_DELIVERY_B2B_LABEL_GET, [
            'request_ids'=>[
                $request_id
            ],
            'generate_type'=>'one'
        ]);

        Storage::disk(OrderProcessValues::SAVE_YANDEX_DELIVERY_FILES_DISK)->put($mw_order_uuid.'.pdf', $pdf);

        return OrderProcessValues::BASE_LABEL_DOMAIN_URI.$mw_order_uuid.'.pdf';
    }

    /**
     * Выполнение результирующих действия после создания заказа от B2B API Яндекса
     *
     * @param string $base_mw_order_number  ID заказа в системе Мой Склад
     * @param string $new_yd_order_id  ID нового созданного заказа в Яндекс.Доставке
     * @return void  Просто выполняем действия и ничего не возвращаем
     */
    public function getOffersPreparedSet(array $offer_set)
    {
        $prepared_offers = [];

        foreach ($offer_set["offers"] as $raw_offer)  {
            $prepared_offers[] = YandexDeliveryB2bOffer::loadFromArray($raw_offer);
        }

        return $prepared_offers;
    }

    /**
     * Функция отправки результирующих данных (маркировки, себестоимости и пр) в ERP
     *
     * @param string $base_mw_order_number  Номер заказа в системе МС
     * @param OrderDocumentData $order_document_data  DTO-объект с данными для заказа
     * @return void  Выполняем действие отправки и ничего не возвращаем
     */
    public function saveReportToMw(string $base_mw_order_number, OrderDocumentData $order_document_data)
    {
        $this->mw_connector->updateDataQuery(OrderProcessValues::MW_ORDER_CHANGE_URL.$base_mw_order_number, [
            'attributes'=>[
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_TC_ORDER_NUMBER,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$order_document_data->new_yd_request_id
                ],
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_YANDEX_LABEL,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$order_document_data->label
                ],
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_ERROR_MESSAGE,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>''
                ]
            ],
            'state'=>[
                'meta'=>[
                    'href'=>OrderProcessValues::MW_ORDER_CHANGE_STATE_URL . YandexDeliveryB2BProcessValues::MW_STATE_YD_ORDER_CREATED,
                    'type'=>'state',
                    'mediaType'=>'application/json'
                ]
            ]
        ]);
    }


    public function sendDeliveryOptionsToMw(string $base_mw_order_number, string $delivery_options_set_json)
    {
        $result = $this->mw_connector->updateDataQuery(OrderProcessValues::MW_ORDER_CHANGE_URL.$base_mw_order_number, [
            'attributes'=>[
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . YandexDeliveryProcessValues::MW_ATTR_COMMENT_UUID,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$delivery_options_set_json
                ],
            ]
        ]);
    }

    /**
     * Функция отправки сообщения об ошибке
     *
     * @return void  Просто ожидание без выполнения действий
     */
    public function sendErrorMessage(string $mw_order_id, string $message)
    {
        $this->mw_connector->updateDataQuery(OrderProcessValues::MW_ORDER_CHANGE_URL.$mw_order_id, [
            'attributes'=>[
                [
                    'meta'=>[
                        'href'=> OrderProcessValues::MW_ORDER_CHANGE_ATTR_URL . OrderProcessValues::MW_ATTR_ERROR_MESSAGE,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$message
                ],
            ],
        ]);
    }


    /**
     * Функция ожидания для синхронизации действий с Яндекс.Доставкой (ожидание формирования документов)
     *
     * @return void  Просто ожидание без выполнения действий
     */
    public function syncYandexDeliveryWaiting()
    {
        sleep(5);
    }
}
