@foreach ($short_options as $option)
    <div class="output-short-option">
        <div class="output-short-option-param">
            Название ТК: {{ $option["partner_name"] }}
        </div>
        <div class="output-short-option-param">
            Стоимость доставки для магазина: {{ $option["delivery_for_sender"] }}
        </div>
        <div class="output-short-option-param">
            Имеющиеся теги: {{ implode(",", $option["tags"]) }}
        </div>
        <div class="output-short-option-param">
            Название тарифа: {{ $option["tariff_name"] }}
        </div>
        <div class="output-short-option-param">
            Самая быстрая дата: {{ $option["min_date"] }}
        </div>
        <div class="output-short-option-param">
            Самая поздняя дата: {{ $option["max_date"] }}
        </div>
    </div>
@endforeach
