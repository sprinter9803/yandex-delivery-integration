<?php

namespace Modules\YandexB2B\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для создания заказа в системе Яндекс Доставки B2B API
 *
 * @author Oleg Pyatin
 */
class YandexDeliveryB2BCreateData extends BaseDto
{
    /**
     * @var Дополнительная информация к заказу (техническая информация)
     */
    public $info;
    /**
     * @var Данные об оплате
     */
    public $billing_info;
    /**
     * @var Данные о месте назначения
     */
    public $destination;
    /**
     * @var Товары в заказе
     */
    public $items;
    /**
     * @var Грузоместа
     */
    public $places;
    /**
     * @var Данные о месте отправки
     */
    public $source;
    /**
     * @var Данные о получателе
     */
    public $recipient_info;
}
