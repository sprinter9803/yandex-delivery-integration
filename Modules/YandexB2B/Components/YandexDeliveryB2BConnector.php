<?php

namespace Modules\YandexB2B\Components;

use Illuminate\Support\Facades\Http;

class YandexDeliveryB2BConnector
{
    public function sendDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. getenv("YANDEX_DELIVERY_B2B_OAUTH_TOKEN"),
        ])->withOptions([
            'verify'=>false
        ])->put(getenv("YANDEX_DELIVERY_B2B_BASE_URL").$uri, $data);

        return $response->json();
    }

    public function sendSimpleQuery(string $uri)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. getenv("YANDEX_DELIVERY_B2B_OAUTH_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->get(getenv("YANDEX_DELIVERY_B2B_BASE_URL").$uri);

        return $response->json();
    }

    public function deleteDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. getenv("YANDEX_DELIVERY_B2B_OAUTH_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->delete(getenv("YANDEX_DELIVERY_B2B_BASE_URL").$uri, $data);

        return $response->json();
    }

    public function sendPostQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. getenv("YANDEX_DELIVERY_B2B_OAUTH_TOKEN"),
        ])->withOptions([
            'verify'=>false
        ])->post(getenv("YANDEX_DELIVERY_B2B_BASE_URL").$uri, $data);

        return $response->json();
    }

    public function processDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. getenv("YANDEX_DELIVERY_B2B_OAUTH_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->post(getenv("YANDEX_DELIVERY_B2B_BASE_URL").$uri, $data);

        return $response->getBody()->getContents();
    }
}
