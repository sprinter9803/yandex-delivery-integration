<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для заполнения данными запроса на отсылку
 *
 * @author Oleg Pyatin
 */
class ShortDeliveryOptionsQuery extends BaseDto
{
    /**
     * @var  ID отправителя
     */
    public $senderId;
    /**
     * @var  Данные об отправителе
     */
    public $from;
    /**
     * @var  Данные о получателе
     */
    public $to;
    /**
     * @var  Данные о габаритах
     */
    public $dimensions;
    /**
     * @var  Данные об отгрузке
     */
    public $shipment;
    /**
     * @var  Данные о стоимости заказа
     */
    public $cost;
    /**
     * @var  Настройки поиска вариантов доставки
     */
    public $settings;
}
