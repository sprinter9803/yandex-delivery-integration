<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для создания заказа в системе Яндекс Доставки
 *
 * @author Oleg Pyatin
 */
class YandexDeliveryCreateData extends BaseDto
{
    /**
     * @var Идентификатор магазина
     */
    public $senderId;
    /**
     * @var Идентификатор заказа в системе партнера
     */
    public $externalId;
    /**
     * @var Комментарий
     */
    public $comment;
    /**
     * @var Тип доставки (COURIER, PICKUP, POST)
     */
    public $deliveryType;
    /**
     * @var Данные о получателе
     */
    public $recipient;
    /**
     * @var Данные о стоимости заказа
     */
    public $cost;
    /**
     * @var Контактные данные
     */
    public $contacts;
    /**
     * @var Данные о вариантах доставки
     */
    public $deliveryOption;
    /**
     * @var Данные об отгрузке
     */
    public $shipment;
    /**
     * @var Данные о грузовых местах заказа
     */
    public $places;
    /**
     * @var Данные о товарах в заказе
     */
    public $items;
}
