<?php

namespace Modules\Order\Entities;

/**
 * Класс для хранения вспомогательной информации к работе с заказами
 *
 * @author Oleg Pyatin
 */
class OrderProcessValues
{
    /**
     * Вес посылки по-умолчанию (полкилограмма) если не указан
     */
    public const PACKAGE_DEFAULT_WEIGHT = 0.5;

    /**
     * UUID атрибута длины в МС
     */
    public const ATTR_ASSORT_LENGTH = '52e3ea13-ecbd-11e8-9107-50480007f4fa';
    /**
     * UUID атрибута ширины в МС
     */
    public const ATTR_ASSORT_WIDTH = '52e3ed5a-ecbd-11e8-9107-50480007f4fb';
    /**
     * UUID атрибута высоты в МС
     */
    public const ATTR_ASSORT_HEIGHT = '52e3ef2b-ecbd-11e8-9107-50480007f4fc';

    /**
     * Атрибут для получения значения ПВЗ
     */
    public const ATTR_PVZ_CODE = '750d08d4-4340-11ea-0a80-02f700046652';
    /**
     * UUID атрибута адреса покупателя
     */
    public const ATTR_RECIPIENT_MW_ORDER_ADDR = '2a5b2325-49cf-11e6-7a69-97110010a6c2';
    /**
     * Аттрибут обозначения что заказ уже оплачен
     */
    public const ATTR_ORDER_IS_PAYED = 'b3417c57-89dd-11e8-9109-f8fc0039ea47';

    /**
     * URL для изменения заказов в МойСклад
     */
    public const MW_ORDER_CHANGE_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/';
    /**
     * URL для изменения статусов заказов в МойСклад
     */
    public const MW_ORDER_CHANGE_STATE_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/states/';
    /**
     * URL для изменения атрибутов заказа в МойСклад (например указания ошибок, накладной ТК и пр)
     */
    public const MW_ORDER_CHANGE_ATTR_URL = 'https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/attributes/';

    /**
     * Атрибут в котором мы указываем номер заказа в системе Транспортной компании (Яндекс.Доставки)
     */
    public const MW_ATTR_TC_ORDER_NUMBER = 'e927642b-88b7-11e7-6b01-4b1d00073fdc';
    /**
     * Атрибут для вывода ошибок
     */
    public const MW_ATTR_ERROR_MESSAGE = 'a31f1635-4fa3-11e9-912f-f3d400039420';
    /**
     * Атрибут для хранения телефона
     */
    public const MW_ATTR_RECIPIENT_PHONE = '9dc79b5e-cb2f-11e3-8493-002590a28eca';

    /**
     * Ссылка на печать штрихкодов
     */
    public const MW_ATTR_YANDEX_LABEL = 'e9a95f3d-4fa2-11e9-9109-f8fc0003db1e';

    /**
     * Вес посылки по-умолчанию (полкилограмма) если не указан
     */
    public const PACKAGE_B2B_DEFAULT_WEIGHT = 500;

    /**
     * Обозначение Laravel-хранилища для сохраняемых от Яндекс.Доставки
     */
    public const SAVE_YANDEX_DELIVERY_FILES_DISK = 'pdf';
    /**
     * Обозначение домена для получения ярлыков
     */
    public const BASE_LABEL_DOMAIN_URI = 'https://i-colors.ru/yandex_delivery/';

    /**
     * Сообщение об успешной массовой выгрузке
     */
    public const YANDEX_B2B_PACKAGE_SUCCESS = "Yandex B2B package load is successfull";
}

