<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\YandexDeliveryOrderCreatingException;

class YandexEmptyWeightException extends YandexDeliveryOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Вес одного из товаров не заполнен';
    }
}
