var output_container = document.querySelector(".emulator-output-container");
var input_form = document.querySelector(".emulator-delivery-options-form");

/**
 * Функция установки AJAX-обработчика на кнопки эмулятора
 */
function tuneActionButtons()
{
    setActionButton(".admin-posts-edit-submit");
}

/**
 * Функция установки AJAX-обработчика на конкретную кнопку
 * @param {type} action_button_selector  Селектор кнопки
 */
function setActionButton(action_button_selector) 
{
    var action_button = document.querySelector(action_button_selector);
    
    if (action_button!== null) {
        action_button.onclick = function(event) {
            event.preventDefault();
            actionGridView(input_form.action);
        }
    }
}

/**
 * AJAX-функция обработки логики GridView
 * 
 * @param {string} href  Путь URL для AJAX-обновления 
 * @returns {undefined}  Ничего не возвращаем 
 */
function actionGridView(href) 
{	
    var xhttp = new XMLHttpRequest();
    
    var formData = new FormData(input_form)
    
    xhttp.open('POST', href, true);
    xhttp.send(formData);

    xhttp.onreadystatechange = function() {

        if ( this.readyState != 4 ) { 
        
            return;
            
        } else {
            
            if (this.responseText) {
                output_container.innerHTML = this.responseText;
            }

            return;
        }
    }
}

tuneActionButtons();
