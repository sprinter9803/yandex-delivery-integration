<?php

namespace Modules\YandexB2B\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания места доставки в Яндекс.Доставке B2B API
 *
 * @author Oleg Pyatin
 */
class CreateDestinationDataB2b extends BaseDto
{
    /**
     * @var Место для доставки курьером
     */
    public $custom_location;
    /**
     * @var Время доставки (передается  UNIX-метками)
     */
    public $interval;
    /**
     * @var Тип места доставки
     */
    public $type;
    /**
     * @var Стоимость
     */
    public $platform_station;
}
