<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\YandexB2B\Entities\CreateDestinationDataB2b;
use Modules\YandexB2B\Entities\CreateDimensionsDataB2b;
use Modules\YandexB2B\Entities\CreatePlaceDataB2b;
use Modules\YandexB2B\Entities\CreateItemsDataB2b;
use Modules\YandexB2B\Entities\CreateRecipientDataB2b;
use Modules\YandexB2B\Entities\CreateInfoDataB2b;
use Modules\YandexB2B\Entities\YandexDeliveryB2BCreateData;
use Modules\YandexB2B\Entities\YandexDeliveryB2BProcessValues;
use Modules\YandexDelivery\Entities\YandexDeliveryProcessValues;
use Modules\MyWarehouse\Services\MwService;
use Modules\Order\Components\DadataConnector;
use Modules\Order\Entities\OrderProcessValues;
use Modules\Order\Entities\DadataAddressFields;
use Carbon\Carbon;

/**
 * Сервис для выполнения распарсивания данных из заказа МС в заказ Яндекс.Доставки средствами B2B API
 *
 * @author Oleg Pyatin
 */
class OrderTransferB2bService
{
    protected $mw_service;

    protected $dadata;

    public function __construct(MwService $mw_service, DadataConnector $dadata)
    {
        $this->mw_service = $mw_service;

        $this->dadata = $dadata;
    }

    /**
     * Функция оформления данных запрос получения оффера (по текущему заказу МС)
     *
     * @param MwOrderData $mw_order_data  Данные заказа МС
     * @return YandexDeliveryB2BCreateData  DTO с данными для оформления
     */
    public function getOrderData(MwOrderData $mw_order_data)
    {
        $new_yandex_b2b_create_data = YandexDeliveryB2BCreateData::loadFromArray([
            'info'=>$this->getInfoData($mw_order_data),
            'billing_info'=>$this->getBillingInfoData(),
            'destination'=>$this->getDestinationData($mw_order_data),
            'source'=>$this->getSourceData(),
            'recipient_info'=>$this->getRecipientData($mw_order_data)
        ]);

        $this->processItemsAndDeliveryData($mw_order_data, $new_yandex_b2b_create_data);

        return $new_yandex_b2b_create_data;
    }

    /**
     * Функция парсинга данных о получателе (покупателе) из данных заказа МС
     *
     * @param MwOrderData $mw_order_data  Данные заказа МС
     * @return CreateRecipientDataB2b  DTO с данными о получателе
     */
    public function getRecipientData(MwOrderData $mw_order_data)
    {
        return CreateRecipientDataB2b::loadFromArray([
            'firstName'=>$mw_order_data->agent["name"],
            'phone'=>$mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_PHONE] ?? $mw_order_data->agent["phone"] ?? ''
        ]);
    }

    /**
     * Функция парсинга технических данных о заказе
     *
     * @param MwOrderData $mw_order_data  Данные заказа МС
     * @return CreateInfoDataB2b  DTO с техническими данными
     */
    public function getInfoData(MwOrderData $mw_order_data)
    {
        return CreateInfoDataB2b::loadFromArray([
            'comment'=>$mw_order_data->attributes[YandexDeliveryProcessValues::MW_ATTR_COMMENT_UUID] ?? '',
            'operator_request_id'=>$mw_order_data->id,  // ! Вернуть при переводе на боевой
//            'operator_request_id'=>$this->getTestingDevelopUniqueOrderNumber()
        ]);
    }

    /**
     * Функция парсинга данных о месте отправки заказа
     *
     * @param MwOrderData $mw_order_data  Данные заказа МС
     * @return array  Массив с данными для отправки
     */
    public function getSourceData()
    {
        return [
            'type'=>YandexDeliveryB2BProcessValues::SOURCE_PLATFORM_TYPE,
            'platform_station'=>[
                'platform_id'=>YandexDeliveryB2BProcessValues::MAIN_PLATFORM_ID
            ],
        ];
    }

    /**
     * Функция выдачи данных по оплате (для накладной)
     *
     * @return array  Массив с данными по оплате
     */
    public function getBillingInfoData()
    {
        $is_order_payed = (bool)($mw_order_data->attributes[OrderProcessValues::ATTR_ORDER_IS_PAYED] ?? false);

        if ($is_order_payed) {
            return [
                'payment_method'=>YandexDeliveryB2BProcessValues::PAYMENT_ALREADY_PAID
            ];
        } else {
            return [
                'payment_method'=>YandexDeliveryB2BProcessValues::PAYMENT_CASH_ON_RECEIPT
            ];
        }
    }

    /**
     * Функция парсинга данных о месте отправки заказа
     *
     * @param MwOrderData $mw_order_data  Данные заказа МС
     * @return CreateDestinationDataB2b  Массив с данными о месте получения заказа
     */
    public function getDestinationData(MwOrderData $mw_order_data)
    {
        $destination = CreateDestinationDataB2b::loadFromArray([
            'interval'=>$this->getDeliveryInterval($mw_order_data)
        ]);

        if ($this->checkAddressTypePvz($mw_order_data)) {

            $destination->type = YandexDeliveryB2BProcessValues::DESTINATION_PLATFORM_TYPE;
            $destination->platform_station = [
                "platform_id"=>$mw_order_data->attributes[OrderProcessValues::ATTR_PVZ_CODE]
            ];

        } else {

            $destination->type = YandexDeliveryB2BProcessValues::DESTINATION_CUSTOM_TYPE;
            $destination->custom_location = [
                "details"=>[
                    "full_address"=>$mw_order_data->attributes[OrderProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR] ?? ''
                ]
            ];
        }

        return $destination;
    }

    /**
     * Функция парсинга данных о товарах, создания грузомест и их связки с товарами
     *
     * @param MwOrderData $mw_order_data  Данные заказа МС
     * @param YandexDeliveryB2BCreateData $yandex_b2b_create_data  Объект с данными заказа Яндекс
     * @return void  Работаем напрямую с DTO заказа без return
     */
    public function processItemsAndDeliveryData(MwOrderData $mw_order_data, YandexDeliveryB2BCreateData $yandex_b2b_create_data)
    {
        $items = [];
        $delivery_places = [];
        $counter = 1;

        $new_delivery_id = $this->generateNewDeliveryId();

        foreach ($mw_order_data->positions["rows"] as $package) {

            $base_price = $package['price'];

            $item_name = $package["id"] ?? 'Товар';

            if (isset($package["assortment"]["name"])) {
                $item_name = preg_replace( '/"([^"]*)"/', "«$1»", $package["assortment"]["name"]);
            }

            $new_delivery_place_id = $this->generateNewDeliveryPlaceId($new_delivery_id, $counter++);
            $dimensions = $this->getItemsDimensionsData($package);

            $delivery_places[] = CreatePlaceDataB2b::loadFromArray([
                'barcode'=>$new_delivery_place_id,
                'physical_dims'=>$dimensions
            ]);

            $items[] = CreateItemsDataB2b::loadFromArray([
                'billing_details'=>[
                    'unit_price'=>$base_price,
                    'assessed_unit_price'=>$base_price
                ],
                'name'=>$item_name,
                'place_barcode'=>$new_delivery_place_id,
                'physical_dims'=>$dimensions,
                'count'=>$package['quantity'],
                'article'=>$package["assortment"]["article"] ?? '',
            ]);
        }

        $yandex_b2b_create_data->items = $items;
        $yandex_b2b_create_data->places = $delivery_places;
    }

    /**
     * Функция получения физических характеристик для выбранного товара
     *
     * @param array $package  Данные о товаре из заказа
     * @return CreateDimensionsDataB2b  Массив с физическими характеристиками (габариты, вес, объем)
     */
    public function getItemsDimensionsData(array $package)
    {
        $assortment_attributes = $this->mw_service->prepareAssortmentAttrs($package["assortment"]["attributes"] ?? []);

        $weight = 0;

        if ($package["assortment"]["meta"]["type"]!=="service") {

            $weight = OrderProcessValues::PACKAGE_B2B_DEFAULT_WEIGHT;
            if (isset($package["assortment"]["weight"])) {
                $weight = (int)($package["assortment"]["weight"] * 1000.0) * $package['quantity'];
            }
        }

        return CreateDimensionsDataB2b::loadFromArray([
            'dx'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_LENGTH] ?? 0,
            'dy'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_WIDTH] ?? 0,
            'dz'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_HEIGHT] ?? 0,
            'predefined_volume'=>$this->processVolumeSize($assortment_attributes[OrderProcessValues::ATTR_ASSORT_LENGTH] ?? 0,
                                                          $assortment_attributes[OrderProcessValues::ATTR_ASSORT_WIDTH] ?? 0,
                                                          $assortment_attributes[OrderProcessValues::ATTR_ASSORT_HEIGHT] ?? 0),
            'weight_gross'=>$weight
        ]);
    }

    /**
     * Проверяем заказ с доставкой через ПВЗ или курьерного типа
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return bool Результат проверки
     */
    public function checkAddressTypePvz(MwOrderData $mw_order_data)
    {
        if (isset($mw_order_data->attributes[OrderProcessValues::ATTR_PVZ_CODE])) {
            return true;
        }
        return false;
    }

    /**
     * Функция расчета объема (в куб.см)
     *
     * @param int $length  Длина товара
     * @param int $width  Ширина товара
     * @param int $height  Высота товара
     *
     * @return int Объем в куб.см
     */
    public function processVolumeSize(int $length, int $width, int $height)
    {
        return $length * $width * $height;
    }


    /**
     * Функция генерации случайного ID для заказа (нужно для группирования грузовых мест по схожести)
     *
     * @return string  Основа для имени грузоместа
     */
    public function generateNewDeliveryId()
    {
        return (string)mt_rand(1,100000).'_'.mt_rand(1,1000).'_'.mt_rand(1,1000);
    }

    /**
     * Функция генерации случайного ID для грузового места
     *
     * @param string $new_delivery_id  Основа в названии грузоместа
     * @param int $number  Конкретный номер грузоместа в заказе
     */
    public function generateNewDeliveryPlaceId(string $new_delivery_id, int $number)
    {
        return $new_delivery_id.'_'.((string)$number);
    }

    /**
     * Функция получения интервалов доставки для имеющегося заказа
     *
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return array Интервал в форме массива
     */
    public function getDeliveryInterval(MwOrderData $mw_order_data)
    {
        // ! Пока интервал от следующего дня до 4-х - требуется уточнение алгоритма

        // Устанавливаем часы с учетом UTC-смещения
        $min_date = (Carbon::now())->add(YandexDeliveryB2BProcessValues::YD_B2B_INTERVAL_DELIVERY_MIN_DAY_OFFSET, 'day');
        $min_date->hour = YandexDeliveryB2BProcessValues::YD_B2B_INTERVAL_DELIVERY_MIN_HOUR;
        $min_date->minute = 0;
        $min_date->second = 0;

        $max_date = (Carbon::now())->add(YandexDeliveryB2BProcessValues::YD_B2B_INTERVAL_DELIVERY_MAX_DAY_OFFSET, 'day');
        $max_date->hour = YandexDeliveryB2BProcessValues::YD_B2B_INTERVAL_DELIVERY_MAX_HOUR;
        $max_date->minute = 0;
        $max_date->second = 0;

        return [
            'from'=>$min_date->timestamp,
            'to'=>$max_date->timestamp
        ];
    }

    /**
     * Функция для получения нового тестового имени заказа (Яндекс.Доставка генерирует ошибки если мы пытаемся оформить по второму
     *     разу уже оформленный заказ из какой-то сторонней системы)
     *
     * @return string Новое тестовое имя заказа
     */
    public function getTestingDevelopUniqueOrderNumber()
    {
        return "SPEC-890-".mt_rand(1,100000);
    }
}
