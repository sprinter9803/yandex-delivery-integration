<?php

namespace Modules\YandexDelivery\Components;

use Illuminate\Support\Facades\Http;

class YandexDeliveryConnector
{
    public function sendDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' OAuth '. getenv("YANDEX_DELIVERY_OAUTH_TOKEN"),
        ])->withOptions([
            'verify'=>false
        ])->put(getenv("YANDEX_DELIVERY_BASE_URL").$uri, $data);

        return $response->json();
    }

    public function sendSimpleQuery(string $uri)
    {
        $response = Http::withHeaders([
            'Authorization'=>' OAuth '. getenv("YANDEX_DELIVERY_OAUTH_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->get(getenv("YANDEX_DELIVERY_BASE_URL").$uri);

        return $response->json();
    }

    public function deleteDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' OAuth '. getenv("YANDEX_DELIVERY_OAUTH_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->delete(getenv("YANDEX_DELIVERY_BASE_URL").$uri, $data);

        return $response->json();
    }

    public function sendPostQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' OAuth '. getenv("YANDEX_DELIVERY_OAUTH_TOKEN"),
        ])->withOptions([
            'verify'=>false
        ])->post(getenv("YANDEX_DELIVERY_BASE_URL").$uri, $data);

        return $response->json();
    }

    public function processDataQuery(string $uri)
    {
        $response = Http::withHeaders([
            'Authorization'=>' OAuth '. getenv("YANDEX_DELIVERY_OAUTH_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->get(getenv("YANDEX_DELIVERY_BASE_URL").$uri);

        return $response->getBody()->getContents();
    }
}
