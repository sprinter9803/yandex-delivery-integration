<?php

namespace Modules\YandexB2B\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных о полученных предложениях доставки от Яндекс B2B API
 *
 * @author Oleg Pyatin
 */
class YandexDeliveryB2bOffer extends BaseDto
{
    /**
     * @var Конкретные параметры оффера
     */
    public $offer_details;
    /**
     * @var ID полученного оффера в системе Яндекс.Доставки
     */
    public $offer_id;
    /**
     * @var Дата актуальности предложения
     */
    public $expires_at;
}
