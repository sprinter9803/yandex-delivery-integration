<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Эмуляторк Яндекс сервисы</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="csrf-param" content="_token" />
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="page">
            <div class="center">
                <main>
                    <div class="main-block">
                        @yield('header')
                    </div>
                    <div class="main-block">
                        @yield('content')
                    </div>
                </main>
            </div>
            <footer class="clearfix">
                <div class="footer-decor-center main-block">
                    <div class="footer-info">
                        Колорс Груп &copy; 2022
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>
