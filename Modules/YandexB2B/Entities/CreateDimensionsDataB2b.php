<?php

namespace Modules\YandexB2B\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания технической размеров товара в Яндекс.Доставке B2B API
 *
 * @author Oleg Pyatin
 */
class CreateDimensionsDataB2b extends BaseDto
{
    /**
     * @var Длина товара
     */
    public $dx;
    /**
     * @var Ширина товара
     */
    public $dy;
    /**
     * @var Высота товара
     */
    public $dz;
    /**
     * @var Приблизительный объем товара
     */
    public $predefined_volume;
    /**
     * @var Приблизительный вес товара
     */
    public $weight_gross;
}
