<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\MyWarehouse\Services\MwService;
use Modules\Order\Services\OrderService;

class YandexFastOption extends Command
{
    protected $signature = 'order:yandex-fast-option';

    protected $description = 'Command for fast creating Yandex B2B orders';

    protected $mw_service;

    protected $order_service;

    public function __construct(MwService $mw_service, OrderService $order_service)
    {
        parent::__construct();
        $this->mw_service = $mw_service;
        $this->order_service = $order_service;
    }

    public function handle()
    {
        $mw_yandex_orders = $this->mw_service->getMwYandexOrders();
        return $this->order_service->createFastOptionOrders($mw_yandex_orders);
    }
}
