<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\YandexDeliveryOrderCreatingException;

class YandexOfferConfirmException extends YandexDeliveryOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Выбранное предложение доставки было отклонено со стороны Яндекс';
    }
}
