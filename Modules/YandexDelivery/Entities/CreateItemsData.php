<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания габаритов заказа в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class CreateItemsData extends BaseDto
{
    /**
     * @var Идентификатор товара в системе партнера
     */
    public $externalId;
    /**
     * @var Название товара
     */
    public $name;
    /**
     * @var Количество единиц товара
     */
    public $count;
    /**
     * @var Цена товара в рублях
     */
    public $price;
    /**
     * @var Объявленная стоимость товара в рублях
     */
    public $assessedValue;
    /**
     * @var Вид налогообложения товара:
     *
         VAT_20 — НДС 20%.
         VAT_10 — НДС 10%.
         VAT_0 — НДС 0%.
         NO_VAT — не облагается НДС.
     */
    public $tax;
    /**
     * @var Вес и габариты отправления
     */
    public $dimensions;
    /**
     * @var ИНН поставщика
     */
    //public $supplierInn;
    /**
     * @var Внешние ID позиций
     */
//    public $placeExternalIds;
}
