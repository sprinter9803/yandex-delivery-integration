<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\MyWarehouse\Services\MwService;
use Modules\Order\Services\OrderService;
use Modules\Order\Entities\ShortDeliveryOptionsQuery;

class OrderController extends Controller
{
    protected $order_service;

    protected $mw_service;

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct(OrderService $order_service, MwService $mw_service)
    {
        $this->order_service = $order_service;
        $this->mw_service = $mw_service;
    }

    /**
     * Действие регистрации заказа в системе Яндекс.Доставка
     * @param $mw_order_uuid  UUID заказа в системе Мой Склад
     * @return Renderable
     */
    public function create(string $mw_order_uuid)
    {
        $mw_order_data = $this->mw_service->getMwOrderData($mw_order_uuid);
        return $this->order_service->createOrder($mw_order_data);
    }

    public function getOrderData(string $yd_order_data)
    {
        return $this->order_service->getOrderData($yd_order_data);
    }

    public function getOffers(string $mw_order_uuid)
    {
        $mw_order_data = $this->mw_service->getMwOrderData($mw_order_uuid);
        return $this->order_service->getOffers($mw_order_data);
    }

    public function confirmOffer(Request $request)
    {
        $yd_offer_id = $request->input('yd_offer_id');
        $mw_order_id = $request->input('mw_order_id');

        return $this->order_service->confirmOffer($yd_offer_id, $mw_order_id);
    }

    public function requestInfo(string $yd_request_id)
    {
        return $this->order_service->requestInfo($yd_request_id);
    }

    public function requestHistory(string $yd_request_id)
    {
        return $this->order_service->requestHistory($yd_request_id);
    }

    public function cancelRequest(string $yd_request_id)
    {
        return $this->order_service->cancelRequest($yd_request_id);
    }

    public function createLabel(Request $request)
    {
        $yd_request_id = $request->input('yd_request_id');
        $mw_order_id = $request->input('mw_order_id');

        return $this->order_service->createLabel($mw_order_id, $yd_request_id);
    }

    public function shortDeliveryOptions(Request $request)
    {
        $short_delivery_options_query = ShortDeliveryOptionsQuery::loadFromArray([
            'to'=>$request->input('to'),
            'dimensions'=>$request->input('dimensions'),
            'shipment'=>$request->input('shipment'),
            'cost'=>$request->input('cost')
        ]);

        return $this->order_service->getShortDeliveryOptions($short_delivery_options_query);
    }

    public function deliveryOptionsErp(string $mw_order_uuid)
    {
        $mw_order_data = $this->mw_service->getMwOrderData($mw_order_uuid);
        return $this->order_service->getDeliveryOptionsToErp($mw_order_data);
    }

    /**
     * Действие для выполнения быстрого оформления заказов в B2B
     *
     * @return string  Сообщение об успешности действий
     */
    public function fastOption()
    {
        $mw_yandex_orders = $this->mw_service->getMwYandexOrders();
        return $this->order_service->createFastOptionOrders($mw_yandex_orders);
    }
}
