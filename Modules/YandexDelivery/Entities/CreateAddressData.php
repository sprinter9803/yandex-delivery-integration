<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для создания заказа в системе Почты России
 *
 * @author Oleg Pyatin
 */
class CreateAddressData extends BaseDto
{
    /**
     * @var Идентификатор населенного пункта
     */
//    public $geoId;
    /**
     * @var Страна
     */
    public $country;
    /**
     * @var Регион, область, округ
     */
    public $region;
    /**
     * @var Субрегион
     */
    public $subRegion;
    /**
     * @var Населенный пункт
     */
    public $locality;
    /**
     * @var Улица
     */
    public $street;
    /**
     * @var Дом
     */
    public $house;
    /**
     * @var Корпус
     */
    public $housing;
    /**
     * @var Строение
     */
    public $building;
    /**
     * @var Номер квартиры или офиса
     */
    public $apartment;
    /**
     * @var Почтовый индекс
     */
    public $postalCode;
    /**
     * @var Почтовый индекс (устарел по документации)
     */
    public $postCode;
}
