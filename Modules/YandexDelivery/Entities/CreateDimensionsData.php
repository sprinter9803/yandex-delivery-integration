<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания габаритов заказа в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class CreateDimensionsData extends BaseDto
{
    /**
     * @var Длина в сантиметрах
     */
    public $length;
    /**
     * @var Высота в сантиметрах
     */
    public $height;
    /**
     * @var Ширина в сантиметрах
     */
    public $width;
    /**
     * @var Вес брутто в килограммах
     */
    public $weight;
}
