<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Order\Services\OrderService;
use Modules\Order\Entities\ShortDeliveryOptionsQuery;

class SiteController extends Controller
{
    protected $order_service;

    public function __construct(OrderService $order_service)
    {
        $this->order_service = $order_service;
    }

    public function apiDocumentation()
    {
        return file_get_contents(storage_path('postman/YandexDeliveryIntegration.postman_collection.json'));
    }

    /**
     * Форма для тестирования
     */
    public function deliveryOptionsEmulator()
    {
        return view('emulator.delivery');
    }

    public function sendShortDeliveryOptions(Request $request)
    {
        $short_delivery_options_query = ShortDeliveryOptionsQuery::loadFromArray([
            'to'=>[
                'location'=>$request->input('destination-address')
            ],
            'dimensions'=>[
                'length'=>$request->input('dimensions-length'),
                'width'=>$request->input('dimensions-width'),
                'height'=>$request->input('dimensions-height'),
                'weight'=>$request->input('dimensions-weight')
            ],
            'shipment'=>[
                'date'=>$request->input('shipment-date'),
                'type'=>$request->input('shipment-type'),
                'includeNonDefault'=>false
            ],
            'cost'=>[
                'assessedValue'=>$request->input('shipment-assessed-value'),
                'itemsSum'=>$request->input('shipment-items-sum'),
                'manualDeliveryForCustomer'=>0,
                'fullyPrepaid'=>false
            ]
        ]);

        $short_delivery_options = $this->order_service->getShortDeliveryOptions($short_delivery_options_query);

        return view('emulator._short_options_output', [
            'short_options'=>$short_delivery_options
        ]);
    }
}
