<?php

namespace Modules\YandexB2B\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания технической информации заказа в Яндекс.Доставке B2B API
 *
 * @author Oleg Pyatin
 */
class CreateInfoDataB2b extends BaseDto
{
    /**
     * @var Комментарий к заказу
     */
    public $comment;
    /**
     * @var ID заказа в ИС клиента
     */
    public $operator_request_id;
}
