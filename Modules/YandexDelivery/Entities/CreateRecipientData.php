<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания информации о получателе в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class CreateRecipientData extends BaseDto
{
    /**
     * @var Имя получателя
     */
    public $firstName;
    /**
     * @var Отчество получателя
     */
    public $middleName;
    /**
     * @var Фамилия получателя
     */
    public $lastName;
    /**
     * @var Адрес электронной почты
     */
    public $email;
    /**
     * @var Адрес получателя
     */
    public $address;
    /**
     * @var Адрес получателя в произвольном формате (При указанном fullAddress параметр address игнорируется)
     */
    public $fullAddress;
    /**
     * @var Идентификатор пункта выдачи (для типов доставки — в пункт выдачи и на почту).
     */
//    public $pickupPointId;
}
