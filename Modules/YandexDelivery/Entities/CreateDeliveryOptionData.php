<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания информации о стоимости в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class CreateDeliveryOptionData extends BaseDto
{
    /**
     * @var Идентификатор тарифа
     */
    public $tariffId;
    /**
     * @var Стоимость доставки (значение округляется до копеек)
     */
    public $delivery;
    /**
     * @var Стоимость доставки для покупателя
     */
    public $deliveryForCustomer;
    /**
     * @var Начальная дата интервала доставки, рассчитанного сервисом
     */
    public $calculatedDeliveryDateMin;
    /**
     * @var Конечная дата интервала доставки, рассчитанного сервисом
     */
    public $calculatedDeliveryDateMax;
    /**
     * @var Идентификатор интервала времени, в который нужно доставить заказ покупателю
     */
    public $deliveryIntervalId;
    /**
     * @var Данные о дополнительных услугах доставки
     */
    public $services;
    /**
     * @var Идентификатор службы доставки
     */
    public $partnerId;
}
