<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания информации о стоимости в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class CreateServicesData extends BaseDto
{
    /**
     * @var Код услуги
     *  Возможные значения:
        DELIVERY — доставка.
        CASH_SERVICE — вознаграждение за перечисление денежных средств.
        SORT — сортировка на едином складе.
        INSURANCE — объявление ценности заказа.
        WAIT_20 — ожидание курьера.
        RETURN — возврат заказа на единый склад.
        RETURN_SORT — сортировка возвращенного заказа.
     */
    public $code;
    /**
     * @var Стоимость услуги.
     */
    public $cost;
    /**
     * @var Услуга оплачивается клиентом
     */
    public $customerPay;
}
