<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для заполнения данными о конкретном варианте доставки
 *
 * @author Oleg Pyatin
 */
class ShortDeliveryOption extends BaseDto
{
    /**
     * @var  Имя партнерской ТК
     */
    public $partner_name;
    /**
     * @var  Стоимость доставки для магазина
     */
    public $delivery_for_sender;
    /**
     * @var  Доступные теги от Яндекса
     */
    public $tags;
    /**
     * @var  Название тарифа
     */
    public $tariff_name;
    /**
     * @var  Наиболее быстрый день доставки
     */
    public $min_date;
    /**
     * @var  Самый поздний возможный день доставки
     */
    public $max_date;
}
