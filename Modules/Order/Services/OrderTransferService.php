<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\YandexDelivery\Entities\CreateAddressData;
use Modules\YandexDelivery\Entities\CreateContactsData;
use Modules\YandexDelivery\Entities\CreateCostData;
use Modules\YandexDelivery\Entities\CreateDeliveryOptionData;
use Modules\YandexDelivery\Entities\CreateDimensionsData;
use Modules\YandexDelivery\Entities\CreateItemsData;
use Modules\YandexDelivery\Entities\CreatePlacesData;
use Modules\YandexDelivery\Entities\CreateRecipientData;
use Modules\YandexDelivery\Entities\CreateServicesData;
use Modules\YandexDelivery\Entities\CreateShipmentData;
use Modules\YandexDelivery\Entities\YandexDeliveryCreateData;
use Modules\YandexDelivery\Entities\YandexDeliveryProcessValues;
use Modules\MyWarehouse\Services\MwService;
use Modules\Order\Components\DadataConnector;
use Modules\Order\Entities\OrderProcessValues;
use Modules\Order\Entities\DadataAddressFields;
use Modules\Order\Entities\ShortDeliveryOptionsQuery;
use Carbon\Carbon;

/**
 * Сервис для выполнения распарсивания данных из заказа МС в заказ Яндекс.Доставки
 *
 * @author Oleg Pyatin
 */
class OrderTransferService
{
    protected $mw_service;

    protected $dadata;

    protected $order_check_service;

    public function __construct(MwService $mw_service, DadataConnector $dadata, OrderCheckService $order_check_service)
    {
        $this->mw_service = $mw_service;

        $this->dadata = $dadata;

        $this->order_check_service = $order_check_service;
    }

    public function getOrderData(MwOrderData $mw_order_data)
    {
        $new_yandex_create_data = YandexDeliveryCreateData::loadFromArray([

            'senderId'=>YandexDeliveryProcessValues::YD_BRAUN_SHOP_SENDER_ID,
            'externalId'=>$this->getTestingDevelopUniqueOrderNumber(),
            'comment'=>$mw_order_data->attributes[YandexDeliveryProcessValues::MW_ATTR_COMMENT_UUID] ?? '',
            'deliveryType'=>YandexDeliveryProcessValues::YD_DELIVERY_TYPE_DEFAULT,

            // Составные параметры
            'recipient'=>$this->getRecipientOrderData($mw_order_data),

            'cost'=>$this->getCostData(),

            'shipment'=>$this->getShipmentData(),

            'contacts'=>$this->getContactsData($mw_order_data),

            'deliveryOption'=>$this->getDeliveryOptionData(),
        ]);

        $this->processItemsAndDeliveryData($mw_order_data, $new_yandex_create_data);

        return $new_yandex_create_data;
    }

    public function getRecipientOrderData(MwOrderData $mw_order_data)
    {
        return CreateRecipientData::loadFromArray([
            'firstName'=>$mw_order_data->agent["name"],
            'middleName'=>'-',
            'lastName'=>'-',
            'email'=>$mw_order_data->agent["email"],
            'address'=>$this->getAddressData($mw_order_data),
            'fullAddress'=>"630000, Новосибирск, Красный проспект, 1, 18"
        ]);
    }




    public function processItemsAndDeliveryData(MwOrderData $mw_order_data, YandexDeliveryCreateData $yandex_create_data)
    {
        $items = [];
        $delivery_places = [];

        $new_delivery_id = $this->generateNewDeliveryId();

        foreach ($mw_order_data->positions["rows"] as $package) {

            $base_price = ((float)$package['price'])/100.0;

            $item_name = $package["id"] ?? 'Товар';

            if (isset($package["assortment"]["name"])) {
                $item_name = preg_replace( '/"([^"]*)"/', "«$1»", $package["assortment"]["name"]);
            }


            $new_delivery_place_id = $this->generateNewDeliveryPlaceId($new_delivery_id);
            $dimensions = $this->getItemsDimensionsData($package);

            $delivery_places[] = CreatePlacesData::loadFromArray([
                'externalId'=>$new_delivery_place_id,
                'dimensions'=>$dimensions
            ]);

            $items[] = CreateItemsData::loadFromArray([
                'externalId'=>$package['id'] ?? '',
                'name'=>$item_name,
                'count'=>$package['quantity'] ?? 1,
                'price'=>$base_price,
                'assessedValue'=>$base_price,
                'tax'=>$this->getVatQuant((int)$package['vat']),
                'dimensions'=>$this->getItemsDimensionsData($package),
            ]);
        }

        $yandex_create_data->items = $items;
        $yandex_create_data->places = $delivery_places;
    }



    public function getShipmentData()
    {
        return CreateShipmentData::loadFromArray([
            'type'=>'IMPORT',
            'date'=>(Carbon::now())->toDateString(),
            'warehouseFrom'=>10004137044,
            'warehouseTo'=>10000030789,
            'partnerTo'=>9
        ]);
    }


    public function getItemsDimensionsData(array $package)
    {
        $assortment_attributes = $this->mw_service->prepareAssortmentAttrs($package["assortment"]["attributes"] ?? []);

        $weight = 0;

        if ($package["assortment"]["meta"]["type"]!=="service") {

            $weight = OrderProcessValues::PACKAGE_DEFAULT_WEIGHT;
            if (isset($package["assortment"]["weight"])) {
                $weight = $package["assortment"]["weight"];
            }
        }

        return CreateDimensionsData::loadFromArray([
            'length'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_LENGTH] ?? 0,
            'width'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_WIDTH] ?? 0,
            'height'=>$assortment_attributes[OrderProcessValues::ATTR_ASSORT_HEIGHT] ?? 0,
            'weight'=>$weight
        ]);
    }

    public function getDeliveryOptionData()
    {
        return CreateDeliveryOptionData::loadFromArray([
            'tariffId'=>100041,
            'delivery'=>100,
            'deliveryForCustomer'=>1,
            'partnerId'=>9,
            'calculatedDeliveryDateMin'=>'2022-02-16',
            'calculatedDeliveryDateMax'=>'2022-02-16',
            'deliveryIntervalId'=>162506449,
            'services'=>[
                [
                    'code'=>'INSURANCE',
                    'cost'=>0.01,
                    'customerPay'=>false
                ],
                [
                    'code'=>'RETURN_SORT',
                    'cost'=>20,
                    'customerPay'=>false
                ],
                [
                    'code'=>'RETURN',
                    'cost'=>488.25,
                    'customerPay'=>false
                ],
                [
                    'code'=>'DELIVERY',
                    'cost'=>651,
                    'customerPay'=>false
                ]
            ],
        ]);
    }

    public function getCostData()
    {
        return CreateCostData::loadFromArray([
            'paymentMethod'=>YandexDeliveryProcessValues::YD_PAYMENT_METHOD_CASH,
            'assessedValue'=>1,
            'fullyPrepaid'=>true,
            'manualDeliveryForCustomer'=>1,
        ]);
    }

    public function getContactsData(MwOrderData $mw_order_data)
    {
        return [
            CreateContactsData::loadFromArray([
                'type'=>YandexDeliveryProcessValues::YD_CONTACT_TYPE_DEFAULT,
                'phone'=>$mw_order_data->agent["phone"],
                'additional'=>'-',
                'firstName'=>'-',
                'middleName'=>'-',
                'lastName'=>'-',
            ])
        ];
    }


    public function getAddressData(MwOrderData $mw_order_data)
    {
        $mw_recipient_address = $mw_order_data->attributes[YandexDeliveryProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR];

        // ! Временно возвращаем тестовые данные, чтобы не загружать Dadata лишний раз
        return CreateAddressData::loadFromArray([
            'country'=>"Россия",
            'region'=>"Новосибирская область",
            "subRegion" => "городской округ Новосибирск",
            'locality'=>"Новосибирск",
            'street'=>"Красный проспект",
            'house'=>"1",
            'building'=>"7",
            'apartment'=>"25",
            'postalCode'=>"630000",
            'postCode'=>"630000"
        ]);

        $dadata_info = $this->tryDadataForAddressProcessing($mw_order_data, $mw_order_data->attributes[YandexDeliveryProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR]);

        return CreateAddressData::loadFromArray([
            'country'=>$dadata_info->country,
            'region'=>$dadata_info->region_with_type,
            'locality'=>$dadata_info->city,
            'street'=>$dadata_info->street_with_type,
            'house'=>$dadata_info->house,
            'building'=>$dadata_info->block,
            'apartment'=>$dadata_info->flat,
            'postalCode'=>$dadata_info->postal_code,
            'postCode'=>$dadata_info->postal_code
        ]);
    }




    public function tryDadataForAddressProcessing(MwOrderData $mw_order_data, string $address)
    {
        $dadata_address = \Arr::get($this->dadata->getCleanAddress($address), '0', []);

        $dadata_info = DadataAddressFields::loadFromArray($dadata_address);

        $full_address = $dadata_info->postal_code . ', ' . $dadata_info->region_with_type . ', ' . $dadata_info->city_with_type . ', ' .$dadata_info->street_with_type;

        $full_address .= ', ' . $dadata_info->house_type_full . ' ' . $dadata_info->house;

        if (!empty($dadata_info->block)) {
            $full_address .= ', ' . $dadata_info->block_type_full . ' ' . $dadata_info->block;
        }

        $full_address .= ', ' . $dadata_info->flat_type . ' ' . $dadata_info->flat;

        // Временно отключаем дополнительные действия в Dadata

        // $address_attrs = $this->order_report_service->getDetailedAddress($mw_order_data, $dadata_info);

        // $this->order_report_service->sendDadataInfo($mw_order_data->id, "Адрес от системы Dadata: ".$full_address, $address_attrs);

        // В случае если в Dadata нету данных о городе - делаем проверку имеется ли о нем уже занесенная запись менеджером в заказе,
        //     Если нет - выходит ошибка
        //if (empty($dadata_info->region_with_type) && empty($dadata_info->city_with_type)) {
        //    $this->order_check_service->checkRecipientAddressFilled($mw_order_data);
        //}

        return $dadata_info;
    }

    public function createAffirmOrderRequest(string $draft_id)
    {
        return [
            "orderIds"=>[
                (int)$draft_id
            ]
        ];
    }

    /**
     * Получение информации о габаритах и стоимости доставки
     */
    public function getDeliveryOptionsItemsInfoData(ShortDeliveryOptionsQuery $delivery_options_query, MwOrderData $mw_order_data)
    {
        $total_cost = 0.0;
        $total_weight = 0.0;

        $common_length = 0;
        $common_width = 0;
        $common_height = 0;

        $is_order_payed = (bool)($mw_order_data->attributes[OrderProcessValues::ATTR_ORDER_IS_PAYED] ?? false);


        foreach ($mw_order_data->positions["rows"] as $package) {

            $price = (((float)$package["price"]) / 100.0);

            $price_discount = $price;
            if (($discount = (int)$package["discount"]) > 0) {
                $price_discount = $price * ( ((float)(100 - $discount)) /100.0);
            }

            $total_cost += ($price_discount * $package['quantity']);


            if ($package["assortment"]["meta"]["type"]!=="service") {

                if (isset($package["assortment"]["weight"])) {
                    $total_weight += $package["assortment"]["weight"] * $package['quantity'];;
                }
            }


            $assortment_attributes = $this->mw_service->prepareAssortmentAttrs($package["assortment"]["attributes"] ?? []);

            $common_length += $assortment_attributes[OrderProcessValues::ATTR_ASSORT_LENGTH] ?? 0;

            if ($assortment_attributes[OrderProcessValues::ATTR_ASSORT_WIDTH]>$common_width) {
                $common_width = $assortment_attributes[OrderProcessValues::ATTR_ASSORT_WIDTH];
            }

            if ($assortment_attributes[OrderProcessValues::ATTR_ASSORT_HEIGHT]>$common_height) {
                $common_height = $assortment_attributes[OrderProcessValues::ATTR_ASSORT_HEIGHT];
            }
        }

        $delivery_options_query->dimensions = [
            'length'=>$common_length,
            'width'=>$common_width,
            'height'=>$common_height,
            'weight'=>$total_weight
        ];

        $delivery_options_query->cost = [
            'assessedValue'=>$total_cost,
            'itemsSum'=>$total_cost,
            'manualDeliveryForCustomer'=>0,
            'fullyPrepaid'=>$is_order_payed
        ];
    }


    public function getDeliveryOptionsShipmentData(MwOrderData $mw_order_data)
    {
        $shipment_type = YandexDeliveryProcessValues::YD_SHIPMENT_TYPE_WITHDRAW;

        if ($this->order_check_service->checkAddressTypePvz($mw_order_data)) {
            $shipment_type = YandexDeliveryProcessValues::YD_SHIPMENT_TYPE_IMPORT;
        }

        return [
            'date'=>(Carbon::now())->toDateString(),
            'type'=>$shipment_type,
            'includeNonDefault'=> YandexDeliveryProcessValues::YD_SHIPMENT_DEFAULT_OPTIONS_INCLUDING
        ];
    }

    public function getDeliveryOptionsDestinationData(MwOrderData $mw_order_data)
    {
        if ($this->order_check_service->checkAddressTypePvz($mw_order_data)) {
            return [

            ];
        } else {
            return [
                'location'=>$mw_order_data->attributes[YandexDeliveryProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR] ?? '',
            ];
        }
    }


    public function getVatQuant(int $vat_value)
    {
        switch ($vat_value) {
            case 20:
                return 'VAT_20';
            case 10:
                return 'VAT_10';
            case 0:
                return 'VAT_0';
            default:
                return 'VAT_20';
        }
    }

    /**
     * Функция для получения нового тестового имени заказа (Для удобства отслеживания в ЛК-почты)
     */
    public function getTestingDevelopUniqueOrderNumber()
    {
        return "TEST-".mt_rand(1,10)."-".mt_rand(1,100);
    }

    /**
     * Функция генерации случайного ID для заказа (нужно для группирования грузовых мест по схожести)
     */
    public function generateNewDeliveryId()
    {
        return (string)mt_rand(1,10000000);
    }

    /**
     * Функция генерации случайного ID для грузового места
     */
    public function generateNewDeliveryPlaceId(string $new_delivery_id)
    {
        return "deliv-".$new_delivery_id."-p-".mt_rand(1,1000)."-".mt_rand(1,10000);
    }
}
