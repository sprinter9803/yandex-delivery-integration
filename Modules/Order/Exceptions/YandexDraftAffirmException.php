<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\YandexDeliveryOrderCreatingException;

class YandexDraftAffirmException extends YandexDeliveryOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Возникли ошибки при подтверждении черновика заказа';
    }
}
