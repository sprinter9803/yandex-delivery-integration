<?php

namespace Modules\YandexB2B\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания информации о грузоместах в Яндекс.Доставке B2B API
 *
 * @author Oleg Pyatin
 */
class CreatePlaceDataB2b extends BaseDto
{
    /**
     * @var Физические характеристики грузоместа
     */
    public $physical_dims;
    /**
     * @var Обозначение грузоместа
     */
    public $barcode;
}
