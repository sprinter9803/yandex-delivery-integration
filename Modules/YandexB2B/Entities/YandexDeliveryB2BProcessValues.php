<?php

namespace Modules\YandexB2B\Entities;

class YandexDeliveryB2BProcessValues
{
    /**
     * URI для получения вариантов доставки у товара
     */
    public const YANDEX_DELIVERY_B2B_GET_ORDER_OFFERS = 'offers/create';
    /**
     * URI для подвтерждения оффера доставки
     */
    public const YANDEX_DELIVERY_B2B_CONFIRM_OFFER_URI = 'offers/confirm';
    /**
     * URI для отмены заказа
     */
    public const YANDEX_DELIVERY_B2B_CANCEL_ORDER = 'request/cancel';
    /**
     * URI для получения информации о заказе
     */
    public const YANDEX_DELIVERY_B2B_REQUEST_INFO = 'request/info?request_id=';
    /**
     * URI для получения истории статусов заказа
     */
    public const YANDEX_DELIVERY_B2B_REQUEST_HISTORY = 'request/history?request_id=';
    /**
     * URI для получения ярлыков к заказу
     */
    public const YANDEX_DELIVERY_B2B_LABEL_GET = 'request/generate-labels';

    /**
     * Обозначение типа склада для данных о месте отправки
     */
    public const SOURCE_PLATFORM_TYPE = 1;
    /**
     * Обозначение типа склада для данных о месте доставки
     */
    public const DESTINATION_PLATFORM_TYPE = 1;
    /**
     * Обозначение типа конкретного места (курьеру) для данных о месте доставки
     */
    public const DESTINATION_CUSTOM_TYPE = 2;

    /**
     * Обозначение предоплаты
     */
    public const PAYMENT_ALREADY_PAID = 'already_paid';
    /**
     * Обозначение оплаты наличкой на месте
     */
    public const PAYMENT_CASH_ON_RECEIPT = 'cash_on_receipt';


    /**
     * Обозначение ID склада для данных о месте отправки (здесь ID склада на Шаболовской) (В боевой среде)
     */
    public const MAIN_PLATFORM_ID = 'aa6e2cdf-f144-4b81-8525-881b9fab6d3d';
    /**
     * Обозначение ID склада на Шаболовской в тестовой среде Яндекса B2B
     */
    public const MAIN_PLATFORM_ID_TEST_ENV = '1fa3fc29-aa63-4b95-839e-261f677f171d';

    /**
     * Обозначение статуса созданного заказа (пока используем статус "В обработке")
     */
    public const MW_STATE_YD_ORDER_CREATED = 'f8ba5795-cb9f-11e3-fefa-002590a28eca';

    /**
     * Значение начального часа для минимального времени у интервала доставки
     */
    public const YD_B2B_INTERVAL_DELIVERY_MIN_HOUR = 6;
    /**
     * Значение сдвига для минимального дня
     */
    public const YD_B2B_INTERVAL_DELIVERY_MIN_DAY_OFFSET = 1;
    /**
     * Значение самого позднего часа для максимально возможного дня у интервала доставки
     */
    public const YD_B2B_INTERVAL_DELIVERY_MAX_HOUR = 19;
    /**
     * Значение сдвига для максимального дня
     */
    public const YD_B2B_INTERVAL_DELIVERY_MAX_DAY_OFFSET = 5;
}
