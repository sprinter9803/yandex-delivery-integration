<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\Order\Services\OrderTransferService;
use Modules\Order\Services\OrderTransferB2bService;
use Modules\Order\Services\OrderReportService;
use Modules\Order\Services\OrderCheckService;
use Modules\YandexDelivery\Components\YandexDeliveryConnector;
use Modules\YandexDelivery\Entities\YandexDeliveryProcessValues;
use Modules\YandexB2B\Entities\YandexDeliveryB2BProcessValues;
use Modules\YandexB2B\Components\YandexDeliveryB2BConnector;
use Modules\Order\Exceptions\YandexDraftAffirmException;
use Modules\Order\Exceptions\YandexOfferCreateException;
use Modules\Order\Exceptions\YandexGetOffersException;
use Modules\Order\Entities\ShortDeliveryOptionsQuery;
use Modules\Order\Entities\ShortDeliveryOption;
use Modules\Order\Entities\OrderProcessValues;
use Illuminate\Support\Str;
use App\Components\ArrayHelper;
use Carbon\Carbon;

/**
 * Сервис для выполнения работы с заказами Почты России
 *
 * @author Oleg Pyatin
 */
class OrderService
{
    protected $order_transfer_service;

    protected $order_report_service;

    protected $order_check_service;

    protected $yandex_delivery_connector;

    protected $yandex_b2b_connector;

    protected $order_transfer_b2b_service;

    public function  __construct(OrderTransferService $order_transfer_service, OrderReportService $order_report_service,
                                OrderCheckService $order_check_service, YandexDeliveryConnector $yandex_delivery_connector,
                                YandexDeliveryB2BConnector $yandex_b2b_connector, OrderTransferB2bService $order_transfer_b2b_service)
    {
        $this->order_transfer_service = $order_transfer_service;
        $this->order_transfer_b2b_service = $order_transfer_b2b_service;
        $this->order_report_service = $order_report_service;
        $this->order_check_service = $order_check_service;

        $this->yandex_delivery_connector = $yandex_delivery_connector;
        $this->yandex_b2b_connector = $yandex_b2b_connector;
    }

    /**
     * Функция получения предложений доставки в Яндекс.Доставке через B2B API (основной вариант)
     *
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return array  Возможные варианты доставки
     */
    public function getOffers(MwOrderData $mw_order_data)
    {
        $this->order_check_service->checkMwOrderDataToB2B($mw_order_data);

        $delivery_create_data = $this->order_transfer_b2b_service->getOrderData($mw_order_data);

        $offers_for_order = $this->yandex_b2b_connector->sendPostQuery(YandexDeliveryB2BProcessValues::YANDEX_DELIVERY_B2B_GET_ORDER_OFFERS, ArrayHelper::toArray($delivery_create_data));

        if ($offer_set = $this->order_check_service->checkOffersGet($offers_for_order)) {
            return ArrayHelper::toArray($this->order_report_service->getOffersPreparedSet($offer_set));
        } else {
            throw new YandexGetOffersException("Get Offers Exception", 0, null, $mw_order_data->id);
        }
    }

    /**
     * Функция подтверждения одного из полученных офферов для Яндекс.Доставки B2B
     *
     * @param string $yd_offer_id  ID оффера доставки в Яндекс.Доставка B2B
     * @return mixed  Данные о результате попытки
     */
    public function confirmOffer(string $yd_offer_id, string $mw_order_id)
    {
        $order_data = $this->yandex_b2b_connector->sendPostQuery(YandexDeliveryB2BProcessValues::YANDEX_DELIVERY_B2B_CONFIRM_OFFER_URI, [
            'offer_id'=>$yd_offer_id
        ]);

        if ($b2b_delivery_order_id = $this->order_check_service->checkConfirmOffer($order_data)) {

            $this->order_report_service->doFinalB2bActions($mw_order_id, $b2b_delivery_order_id);
            return $b2b_delivery_order_id;

        } else {

            throw new YandexOfferAffirmException("Offer Affirm Exception", 0, null, $mw_order_id);
        }
    }


    /**
     * Функция получения информации о заявке (заказе) в системе Яндекс.Доставка B2B
     *
     * @param string $yd_request_id  ID заказа Яндекс.Доставки
     * @return mixed  Данные о заказе в Яндекс.Доставке
     */
    public function requestInfo(string $yd_request_id)
    {
        return $this->yandex_b2b_connector->sendSimpleQuery(YandexDeliveryB2BProcessValues::YANDEX_DELIVERY_B2B_REQUEST_INFO . $yd_request_id);
    }

    /**
     * Функция получения информации об истории статусов заказов в системе Яндекс.Доставка B2B
     *
     * @param string $yd_request_id  ID заказа Яндекс.Доставки
     * @return mixed  Данные об истории статусов заказа
     */
    public function requestHistory(string $yd_request_id)
    {
        return $this->yandex_b2b_connector->sendSimpleQuery(YandexDeliveryB2BProcessValues::YANDEX_DELIVERY_B2B_REQUEST_HISTORY . $yd_request_id);
    }

    /**
     * Функция отмена заказа в системе Яндекс.Доставка B2B
     *
     * @param string $yd_request_id  ID заказа Яндекс.Доставка
     * @return mixed  Данные об успешности отмены заказа
     */
    public function cancelRequest(string $yd_request_id)
    {
        return $this->yandex_b2b_connector->sendPostQuery(YandexDeliveryB2BProcessValues::YANDEX_DELIVERY_B2B_CANCEL_ORDER, [
            'request_id'=>$yd_request_id
        ]);
    }






    /**
     * Функция создания заказа в системе Яндекс.Доставки через стандартное API (на момент создания не актуально, потому что используется B2B версия),
     *     выполняется в 2 шага - оформление черновика заказа и его подтверждение
     *
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return string  ID сформированного заказа в Яндекс.Доставке
     */
    public function createOrder(MwOrderData $mw_order_data)
    {
        $this->order_check_service->checkMwOrderData($mw_order_data);

        $yandex_delivery_order_create_data = $this->order_transfer_service->getOrderData($mw_order_data);

        $draft_create_result = $this->yandex_delivery_connector->sendPostQuery(YandexDeliveryProcessValues::YANDEX_DELIVERY_ORDER_CREATE_URI, ArrayHelper::toArray($yandex_delivery_order_create_data));

        if ($this->order_check_service->checkDraftCreateResult($draft_create_result)) {

            // Подтверждение черновика заказа в обычный заказ (Прохождение ряда дополнительных валидаций)
            $order_affirm_result = $this->yandex_delivery_connector->sendPostQuery(YandexDeliveryProcessValues::YANDEX_DELIVERY_ORDER_AFFIRM_URI,
                                        $this->order_transfer_service->createAffirmOrderRequest($draft_create_result));

            if ($new_yd_order_id = $this->order_check_service->checkDraftAffirmResult($order_affirm_result)) {

                $this->order_report_service->doFinalActions($mw_order_data->id, $new_yd_order_id);

            } else {

                throw new YandexDraftAffirmException("Draft not affirmed to order", 0, null, $mw_order_data->id);
            }

        } else {

            throw new YandexOfferCreateException("Draft wasn`t created", 0, null, $mw_order_data->id);
        }

        return $new_yd_order_id;
    }

    /**
     * Функция вывода данных о заказе в системе Яндекс.Доставки (Стандартное API)
     *
     * @param string $yd_order_data  Данные о заказе Яндекс.Доставки
     * @return string  ID сформированного заказа в Яндекс.Доставке
     */
    public function getOrderData(string $yd_order_data)
    {
        return $this->yandex_delivery_connector->sendSimpleQuery(YandexDeliveryProcessValues::YANDEX_DELIVERY_ORDER_GET_INFO_URI.$yd_order_data);
    }

    /**
     * Функция создания ярлыка по запросу
     *
     * @param string $mw_order_uuid  UUID заказа МС
     * @param string $yd_request_id  ID заказа в системе Яндекс.Доставки
     * @return string  ID сформированного заказа в Яндекс.Доставке
     */
    public function createLabel(string $mw_order_uuid, string $yd_request_id)
    {
        return $this->order_report_service->getYandexDeliveryLabel($mw_order_uuid, $yd_request_id);
    }


    public function getShortDeliveryOptions(ShortDeliveryOptionsQuery $short_delivery_options_query)
    {
        $short_options_set = [];

        $short_delivery_options_query->senderId = YandexDeliveryProcessValues::YD_BRAUN_SHOP_SENDER_ID;
        $short_delivery_options_query->from = [
            'location'=>YandexDeliveryProcessValues::YD_DEFAULT_WAREHOUSE_LOCATION
        ];
        $short_delivery_options_query->settings = $this->getDeliveryOptionsSettings();

        $delivery_options = $this->yandex_delivery_connector->sendDataQuery(YandexDeliveryProcessValues::YANDEX_DELIVERY_OPTIONS_LIST,
                ArrayHelper::toArray($short_delivery_options_query));

        if ($this->order_check_service->checkDeliveryOptions($delivery_options)) {

            foreach ($delivery_options as $option) {

                $max_date = (Carbon::createFromFormat('Y-m-d', $option["delivery"]["calculatedDeliveryDateMax"]));
                $min_date = (Carbon::createFromFormat('Y-m-d', $option["delivery"]["calculatedDeliveryDateMin"]));

                $calculated_date_diff = $max_date->diffInDays($min_date) + 1;

                $short_options_set[] = ShortDeliveryOption::loadFromArray([
                    'partner_name'=>$option["delivery"]["partner"]["name"],
                    'delivery_for_sender'=>$option["cost"]["deliveryForSender"],
                    'min_date'=>$option["delivery"]["calculatedDeliveryDateMin"],
                    'max_date'=>$option["delivery"]["calculatedDeliveryDateMax"],
                    'tariff_name'=>$option["tariffName"],
                    'tags'=>$option["tags"]
                ]);
            }

        } else {

            return [];
        }

        return ArrayHelper::toArray($short_options_set);
    }



    public function getDeliveryOptionsToErp(MwOrderData $mw_order_data)
    {
        $short_options_set = [];

        $short_delivery_options_query = ShortDeliveryOptionsQuery::loadFromArray([
            'senderId'=>YandexDeliveryProcessValues::YD_BRAUN_SHOP_SENDER_ID,
            'from'=>[
                'location'=>YandexDeliveryProcessValues::YD_DEFAULT_WAREHOUSE_LOCATION
            ],
            'settings'=>$this->getDeliveryOptionsSettings(),
            'to'=>$this->order_transfer_service->getDeliveryOptionsDestinationData($mw_order_data),
            'shipment'=>$this->order_transfer_service->getDeliveryOptionsShipmentData($mw_order_data),
        ]);

        $this->order_transfer_service->getDeliveryOptionsItemsInfoData($short_delivery_options_query, $mw_order_data);

        $delivery_options = $this->yandex_delivery_connector->sendDataQuery(YandexDeliveryProcessValues::YANDEX_DELIVERY_OPTIONS_LIST,
                ArrayHelper::toArray($short_delivery_options_query));

        if ($this->order_check_service->checkDeliveryOptions($delivery_options)) {

            foreach ($delivery_options as $option) {

                $short_options_set[] = ShortDeliveryOption::loadFromArray([
                    'partner_name'=>$option["delivery"]["partner"]["name"],
                    'deliveryForSender'=>$option["cost"]["deliveryForSender"],
                    'min_date'=>$option["delivery"]["calculatedDeliveryDateMin"],
                    'max_date'=>$option["delivery"]["calculatedDeliveryDateMax"],
                    'tariff_name'=>$option["tariffName"],
                    'tags'=>$option["tags"]
                ]);
            }

        } else {
            return YandexDeliveryProcessValues::YD_DELIVERY_OPTIONS_ERROR;
        }

        $this->order_report_service->sendDeliveryOptionsToMw($mw_order_data->id, json_encode(ArrayHelper::toArray($short_options_set)));

        return YandexDeliveryProcessValues::YD_DELIVERY_OPTIONS_SUCCESS_SEND;
    }


    /**
     * Функция создания заказов B2B в быстром варианте
     *
     * @param array $mw_yandex_taxi_orders  Список заказов в форме DTO
     * @return void  Просто выполняем создание, ничего не возвращаем
     */
    public function createFastOptionOrders(array $mw_yandex_taxi_orders)
    {
        foreach ($mw_yandex_taxi_orders as $order) {

            // Выполнение первичных валидаций
            if ($this->order_check_service->checkNonBlockMwOrderDataToB2B($order)) {
                continue;
            }

            $delivery_create_data = $this->order_transfer_b2b_service->getOrderData($order);

            $offers_for_order = $this->yandex_b2b_connector->sendPostQuery(YandexDeliveryB2BProcessValues::YANDEX_DELIVERY_B2B_GET_ORDER_OFFERS, ArrayHelper::toArray($delivery_create_data));

            if ($offer_set = $this->order_check_service->checkOffersGet($offers_for_order)) {

                $offer_prepared_set = ArrayHelper::toArray($this->order_report_service->getOffersPreparedSet($offer_set));

                $fast_option_offer_id = $this->getFastOptionOrderRequestId($offer_prepared_set);

                $order_data = $this->yandex_b2b_connector->sendPostQuery(YandexDeliveryB2BProcessValues::YANDEX_DELIVERY_B2B_CONFIRM_OFFER_URI, [
                    'offer_id'=>$fast_option_offer_id
                ]);

                if ($b2b_delivery_order_id = $this->order_check_service->checkConfirmOffer($order_data)) {

                    $this->order_report_service->doFinalB2bActions($order->id, $b2b_delivery_order_id);

                } else {
                    $this->order_report_service->sendErrorMessage($order->id, "Ошибка при оформлении оффера");
                }

            } else {
                $this->order_report_service->sendErrorMessage($order->id, "Ошибка при получении списка офферов");
            }
        }

        echo OrderProcessValues::YANDEX_B2B_PACKAGE_SUCCESS;
    }

    /**
     * Функция получения ID наиболее быстрого варианта оформления
     *
     * @param array $offer_set  Массив имеющихся предложений
     * @return string  UUID конкретного предложения
     */
    public function getFastOptionOrderRequestId(array $offer_set)
    {
        $offers = collect($offer_set);

        $prepared_offers = $offers->map(function ($item, $key) {
            return [
                'offer_id'=>$item["offer_id"],
                'time'=>$item["offer_details"]["delivery_interval"]["min"]
            ];
        });

        $earliest_offer = $prepared_offers->first();

        $current_time_label = Carbon::create($earliest_offer["time"]);
        $current_offer_id = $earliest_offer["offer_id"];

        foreach ($prepared_offers as $offer) {

            $next_time_label = Carbon::create($offer["time"]);

            if ($current_time_label->greaterThan($next_time_label)) {
                $current_time_label = $next_time_label;
                $current_offer_id = $offer["offer_id"];
            }
        }

        return $current_offer_id;
    }

    public function getDeliveryOptionsSettings()
    {
        return [
            'useHandlingTime'=>false,
            'useWarehouseSchedule'=>false,
            'showDisabledOptions'=>true
        ];
    }
}
