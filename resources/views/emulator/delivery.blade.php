@extends('layouts.emulator')

@section('header')
    <h2 class='emulator-head-label'>Проверка вариантов доставки</h2>
@endsection

@section('content')
    <script src="{{ asset("js/emulator_delivery.js") }}" defer></script>

    <div class="emulator-input-container">
        <h3>Входные данные</h3>
        <form class="emulator-delivery-options-form" method="POST" action="{{ route("emulator.send_delivery_options") }}"  enctype="multipart/form-data">
            @csrf

            <div class="emulator-input-row">
                <div class="emulator-input-block">
                    <h3>Точка назначения</h3>
                    <div class="emulator-input-block-field">
                        <h4>Место назначения</h4>
                        <input name="destination-address" type="text" value="Красногорск, Павшинский бульвар, 18, 6" class="long-field">
                    </div>
                </div><!--
             --><div class="emulator-input-block">
                    <h3>Физические характеристики посылки</h3>
                    <div class="emulator-input-block-line-field">
                        <h4>Длина</h4>
                        <input name="dimensions-length" type="text" value="10">
                    </div>
                    <div class="emulator-input-block-line-field">
                        <h4>Ширина</h4>
                        <input name="dimensions-width" type="text" value="20">
                    </div>
                    <div class="emulator-input-block-line-field">
                        <h4>Высота</h4>
                        <input name="dimensions-height" type="text" value="30">
                    </div>
                    <div class="emulator-input-block-line-field">
                        <h4>Вес</h4>
                        <input name="dimensions-weight" type="text" value="5.25">
                    </div>
                </div>
            </div>

            <div class="emulator-input-row">
                <div class="emulator-input-block">
                    <h3 class="vertical-offset">Данные о доставке</h3>
                    <div class="emulator-input-block-line-field">
                        <h4>Дата отгрузки</h4>
                        <input name="shipment-date" type="text" value="{{ date("Y-m-d") }}">
                    </div>
                    <div class="emulator-input-block-line-field">
                        <h4>Тип отгрузки (IMPORT, WITHDRAW)</h4>
                        <input name="shipment-type" type="text" value="IMPORT">
                    </div>
                </div><!--
             --><div class="emulator-input-block">
                    <h3 class="vertical-offset">Стоимость и оплата</h3>
                    <div class="emulator-input-block-line-field">
                        <h4>Объявленная стоимость заказа</h4>
                        <input name="shipment-assessed-value" type="text" value="200">
                    </div>
                    <div class="emulator-input-block-line-field">
                        <h4>Суммарная стоимость товаров</h4>
                        <input name="shipment-items-sum" type="text" value="200">
                    </div>
                </div>
            </div>

            <div class="emulator-input-button-block">
                <input type="submit" value="Получить варианты" class="admin-posts-edit-submit">
            </div>
        </form>
    </div>

    <div class="emulator-output-container">

    </div>
@endsection
