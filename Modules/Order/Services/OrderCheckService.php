<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\YandexDelivery\Entities\YandexDeliveryProcessValues;
use Modules\Order\Exceptions\YandexRecipientNotFullContactsException;
use Modules\Order\Exceptions\YandexDeliveryAddressEmptyException;
use Modules\Order\Exceptions\YandexEmptyWeightException;
use Modules\Order\Exceptions\YandexEmptyQuantityException;
use Modules\Order\Services\OrderReportService;
use Modules\Order\Entities\OrderProcessValues;

/**
 * Сервис для организации логики проверки данных
 *
 * @author Oleg Pyatin
 */
class OrderCheckService
{
    protected $order_report_service;

    public function __construct(OrderReportService $order_report_service)
    {
        $this->order_report_service = $order_report_service;
    }

    /**
     * Функция проверки корректности данных заказа МойСКлад (для обычного API Яндекса)
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkMwOrderData(MwOrderData $mw_order_data)
    {
        $this->checkRecipientContacts($mw_order_data);

        $this->checkDeliveryAddress($mw_order_data);
    }

    /**
     * Проверка корректного заполнения данных для пользователя (телефона)
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     */
    public function checkRecipientContacts(MwOrderData $mw_order_data)
    {
        if (empty($mw_order_data->agent["phone"]) && empty($mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_PHONE])) {
            throw new YandexRecipientNotFullContactsException("No info about phone", 0, null, $mw_order_data->id);
        }
    }

    /**
     * Проверка что заполнен адрес доставки (или ПВЗ)
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkDeliveryAddress(MwOrderData $mw_order_data)
    {
        if (empty($mw_order_data->attributes[YandexDeliveryProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR])
                && empty($mw_order_data->attributes[OrderProcessValues::ATTR_PVZ_CODE])) {

            throw new YandexDeliveryAddressEmptyException("No info about address", 0, null, $mw_order_data->id);
        }
    }

    /**
     * Проверка что нет товаров у которых не указан weight
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkEmptyWeight(MwOrderData $mw_order_data)
    {
        foreach ($mw_order_data->positions["rows"] as $package) {
            if (!isset($package["assortment"]["weight"])) {
                throw new YandexEmptyWeightException("There is items without declared weight", 0, null, $mw_order_data->id);
            }
        }
    }

    /**
     * Проверка что нет товаров у которых не указано количество
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkEmptyQuantity(MwOrderData $mw_order_data)
    {
        foreach ($mw_order_data->positions["rows"] as $package) {
            if (empty($package['quantity'])) {
                throw new YandexEmptyQuantityException("There is items without declared quantity", 0, null, $mw_order_data->id);
            }
        }
    }

    /**
     * Проверка создания черновика заказа для обычного API Яндекс
     *
     * @param mixed $draft_create_result  Ответное сообщение для API после запроса
     * @return boolean  Возвращаем результат проверки
     */
    public function checkDraftCreateResult($draft_create_result)
    {
        if (is_int($draft_create_result)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверка подтверждения черновика заказа в полноценный для обычного API Яндекс
     *
     * @param mixed $order_affirm_result  Ответное сообщение для API после запроса
     * @return mixed  Возвращаем ID закаща если все правильно и false если нет
     */
    public function checkDraftAffirmResult($order_affirm_result)
    {
        if ($order_affirm_result[0]['status']==='SUCCESS') {
            return $order_affirm_result[0]['orderId'];
        } else {
            return false;
        }
    }


    /**
     * Функция проверки корректности данных заказа МойСклад
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkMwOrderDataToB2B(MwOrderData $mw_order_data)
    {
        $this->checkRecipientContacts($mw_order_data);

        $this->checkDeliveryAddress($mw_order_data);

        $this->checkEmptyWeight($mw_order_data);

        $this->checkEmptyQuantity($mw_order_data);
    }

    /**
     * Функция проверки полуечния предложений о заказе от Яндекс.Доставки B2B API
     *
     * @param MwOrderData $yandex_offers_set  Полученный ответ от API
     * @return mixed  Возвращаем набор предложений если все правильно и false если нет
     */
    public function checkOffersGet(array $yandex_offers_set)
    {
        if (!isset($yandex_offers_set["error_details"]) && !isset($yandex_offers_set["message"])) {
            return $yandex_offers_set;
        } else {
            return false;
        }
    }

    /**
     * Проверка успешного подтверждения предложения для B2B API
     *
     * @param MwOrderData $offer_confirm_result  Массив ответа от API
     * @return void  Возвращаем Request ID заказа если все правильно и false если нет
     */
    public function checkConfirmOffer(array $offer_confirm_result)
    {
        return $offer_confirm_result["request_id"] ?? false;
    }

    /**
     * Проверка успешного получения списка опций доставки
     *
     * @param array $delivery_options_array  Массив ответа от API
     * @return void  Возвращаем Request ID заказа если все правильно и false если нет
     */
    public function checkDeliveryOptions(array $delivery_options_array)
    {
        if (!empty($delivery_options_array[0]["tariffName"])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверяем заказ с доставкой через ПВЗ или курьерного типа
     * @param MwOrderData $mw_order_data  Данные о заказе МС
     * @return bool Результат проверки
     */
    public function checkAddressTypePvz(MwOrderData $mw_order_data)
    {
        if (isset($mw_order_data->attributes[OrderProcessValues::ATTR_PVZ_CODE])) {
            return true;
        }
        return false;
    }




    /**
     * Функция неблокирующей проверки корректности данных заказа МойСклад
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkNonBlockMwOrderDataToB2B(MwOrderData $mw_order_data)
    {
        if ($this->checkNonBlockRecipientContacts($mw_order_data)) {
            return true;
        }

        if ($this->checkNonBlockDeliveryAddress($mw_order_data)) {
            return true;
        }

        if ($this->checkNonBlockEmptyWeight($mw_order_data)) {
            return true;
        }

        if ($this->checkEmptyQuantity($mw_order_data)) {
            return true;
        }
    }

    public function checkNonBlockRecipientContacts(MwOrderData $mw_order_data)
    {
        if (empty($mw_order_data->agent["phone"]) && empty($mw_order_data->attributes[OrderProcessValues::MW_ATTR_RECIPIENT_PHONE])) {
            $this->order_report_service->sendErrorMessage($mw_order_data->id, "В заказе не указан телефон");
            return true;
        } else {
            return false;
        }
    }

    public function checkNonBlockDeliveryAddress(MwOrderData $mw_order_data)
    {
        if (empty($mw_order_data->attributes[YandexDeliveryProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR])
                && empty($mw_order_data->attributes[OrderProcessValues::ATTR_PVZ_CODE])) {

            $this->order_report_service->sendErrorMessage($mw_order_data->id, "В заказе не указан адрес получателя");
            return true;
        } else {
            return false;
        }
    }

    public function checkNonBlockEmptyWeight(MwOrderData $mw_order_data)
    {
        foreach ($mw_order_data->positions["rows"] as $package) {
            if (!isset($package["assortment"]["weight"])) {
                $this->order_report_service->sendErrorMessage($mw_order_data->id, "Имеются товары без указанного веса");
                return true;
            }
        }
        return false;
    }

    public function checkNonBlockEmptyQuantity(MwOrderData $mw_order_data)
    {
        foreach ($mw_order_data->positions["rows"] as $package) {
            if (empty($package['quantity'])) {
                $this->order_report_service->sendErrorMessage($mw_order_data->id, "Имеются товары без указанного количества");
                return true;
            }
        }
        return false;
    }
}
