<?php

namespace Modules\YandexB2B\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания технической информации заказа в Яндекс.Доставке B2B API
 *
 * @author Oleg Pyatin
 */
class CreateItemsDataB2b extends BaseDto
{
    /**
     * @var Количество товара
     */
    public $count;
    /**
     * @var Название товара
     */
    public $name;
    /**
     * @var Артикул
     */
    public $article;
    /**
     * @var Стоимость
     */
    public $billing_details;
    /**
     * @var Характеристики
     */
    public $physical_dims;
    /**
     * @var Грузоместо
     */
    public $place_barcode;
}
