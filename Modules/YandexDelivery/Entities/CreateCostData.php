<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания информации о стоимости в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class CreateCostData extends BaseDto
{
    /**
     * @var Способ оплаты заказа
     */
    public $paymentMethod;
    /**
     * @var Объявленная стоимость заказа
     */
    public $assessedValue;
    /**
     * @var Заказ полностью предоплачен.
     */
    public $fullyPrepaid;
    /**
     * @var Стоимость доставки для покупателя (определяется магазином)
     */
    public $manualDeliveryForCustomer;
}
