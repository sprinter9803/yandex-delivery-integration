<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\YandexDeliveryOrderCreatingException;

class YandexGetOffersException extends YandexDeliveryOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Ошибка на шаге получения вариантов доставки от Яндекс';
    }
}
