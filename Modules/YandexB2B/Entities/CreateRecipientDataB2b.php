<?php

namespace Modules\YandexB2B\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания информации о получателе в Яндекс.Доставке B2B API
 *
 * @author Oleg Pyatin
 */
class CreateRecipientDataB2b extends BaseDto
{
    /**
     * @var Имя получателя
     */
    public $firstName;
    /**
     * @var Телефон получателя
     */
    public $phone;
}
