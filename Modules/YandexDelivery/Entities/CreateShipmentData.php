<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания типа отгрузки в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class CreateShipmentData extends BaseDto
{
    /**
     * @var Тип отгрузки
     *          IMPORT - самостоятельно
     *          WITHDRAW - курьером
     */
    public $type;
    /**
     * @var Дата отгрузки в формате YYYY-MM-DD
     */
    public $date;
    /**
     * @var Идентификатор склада, с которого отгружаются товары
     */
    public $warehouseFrom;
    /**
     * @var Идентификатор склада, на который отгружаются товары
     */
    public $warehouseTo;
    /**
     * @var Идентификатор партнера, которому отгружаются товары
     */
    public $partnerTo;
}
