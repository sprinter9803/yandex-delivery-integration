<?php

namespace Modules\YandexDelivery\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для указания контактных данных в Яндекс.Доставке
 *
 * @author Oleg Pyatin
 */
class CreateContactsData extends BaseDto
{
    /**
     * @var Тип контакта
     */
    public $type;
    /**
     * @var Номер телефона
     */
    public $phone;
    /**
     * @var Дополнительный номер телефона
     */
    public $additional;
    /**
     * @var Имя контакта
     */
    public $firstName;
    /**
     * @var Отчество контакта
     */
    public $middleName;
    /**
     * @var Фамилия контакта
     */
    public $lastName;
}
