<?php

namespace Modules\Order\Components;

use Illuminate\Support\Facades\Http;

/**
 * Компонент для выполнения запросов к сервису Dadata
 *
 * @author Oleg Pyatin
 */
class DadataConnector
{
    /**
     * URL для получения информации об адресе
     */
    public const URI_CLEAN_ADDRESS = "https://cleaner.dadata.ru/api/v1/clean/address";
    /**
     * URL для получения данных о городе от транспортных компаний по его КЛАДР
     */
    public const URI_TRANSPORT_COMPANY_CODES = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/delivery";

    /**
     * Функция получения подробной информации об адресе с помощью сервиса Dadata
     * @param string $address  Нужный адрес
     * @return array  Массив с данными
     */
    public function getCleanAddress(string $address)
    {
        return Http::withHeaders([
            'Authorization'=>'Token '.getenv("DADATA_TOKEN"),
            "X-Secret" => ' '.getenv("DADATA_SECRET")
        ])->withOptions([
            'verify'=>false
        ])->post(static::URI_CLEAN_ADDRESS, [
            $address
        ])->json();
    }

    /**
     * Функция для получения информации транспортных компаний о городе по его КЛАДР
     * @param string $city_kladr_id  КЛАДР нужного города
     * @return array Массив данных от компаний (СДЕК, DPD, BoxBerry)
     */
    public function getTransportCompanyCode(string $city_kladr_id)
    {
        return Http::withHeaders([
            'Authorization'=>'Token '.getenv("DADATA_TOKEN"),
            "X-Secret" => ' '.getenv("DADATA_SECRET")
        ])->withOptions([
            'verify'=>false
        ])->post(static::URI_TRANSPORT_COMPANY_CODES, [
            "query"=>$city_kladr_id,
            "count"=>1
        ])->json();
    }
}
