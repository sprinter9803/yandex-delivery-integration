<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения результирующих данных о заказе (ярлыки и другие документы)
 *
 * @author Oleg Pyatin
 */
class OrderDocumentData extends BaseDto
{
    /**
     * @var  Новый заказ в системе Яндекс.Доставка B2B API
     */
    public $new_yd_request_id;
    /**
     * @var  Ссылка на ярлык
     */
    public $label;
}
